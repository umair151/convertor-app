package com.example.dev.convertorapp;

/**
 * Created by dev on 4/28/17.
 */

public class Conversions {

    public static final String toTbsp = "TeaSpoon - TableSpoon";
    public static final String toMilli = "Liter - Milliliter";
    public static final String toQuart = "Pint - Quart";
    public static final String toOunce = "Pound - Ounce";
    public static final String toKg = "Gram - Kilogram";
    public static final String toCm = "Inch - Centimeter";

    public static double toTbsp(double val){
        return val * 0.333 ;
    }

    public static double toMilli(double val){
        return val * 1000;
    }

    public static double toQuart(double val){
        return val * 0.5;
    }

    public static double toOunce(double val){
        return val * 16;
    }

    public static double toKg(double val){
        return val * 0.001;
    }

    public static double toCenti(double val){
        return val * 2.54;
    }

    /*Vice versa conversion methods */

    public static double toTsp(double val){
        return val*3;
    }

    public static double toLiter(double val){
        return val * 0.001;
    }

    public static double toPint(double val){
        return val * 2;
    }

    public static double toPound(double val){
        return val * 0.0625;
    }

    public static double toGram(double val){
        return val * 1000;
    }

    public static double toInch(double val){
        return val * 0.393701;
    }
}
